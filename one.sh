#!/bin/bash

set -x

serverless create --template aws-python3 --name pyohio-demo --path ./pyohio-demo
